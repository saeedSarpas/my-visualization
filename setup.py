from setuptools import setup
from distutils.command.build_py import build_py as _build_py
import os

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name='my_visualization',
    version='0.0',
    description='A simple wrapper around matplotlib',
    long_description=read('README.md'),
    url='http://gitlab.com/saeedSarpas/my-visualization',
    keywords='RADAMESH',
    author='Saeed Sarpas',
    author_email='saeed.sarpas@phys.ethz.ch',
    license='GPLv3',
    packages=['my_visualization'],
    install_requires=[
        'matplotlib',
        'numpy',
    ],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    zip_safe=False
)
